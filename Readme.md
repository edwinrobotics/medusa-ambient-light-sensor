# Medusa Ambient Light Sensor

![Medusa Ambient Light Sensor](https://shop.edwinrobotics.com/4055-thickbox_default/medusa-ambient-light-sensor.jpg "Medusa Ambient Light Sensor")

[Medusa Ambient Light Sensor](https://shop.edwinrobotics.com/modules/1210-medusa-ambient-light-sensor.html)

The Medusa Ambient Light sensor is a plug and play board for any application which needs the detection of light in visible spectrum as well as infrared. The SFH 3710 is a phototransistor, so greater the intensity of light falling on the sensor the higher the output voltage.


Repository Contents
-------------------
* **/Design** - Eagle Design Files (.brd,.sch)
* **/Panel** - Panel file used for production

License Information
-------------------
The hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Distributed as-is; no warranty is given.